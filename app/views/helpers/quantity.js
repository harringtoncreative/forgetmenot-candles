'use strict';
const Handlebars = require('handlebars');

module.exports = function(){
    Handlebars.registerHelper('quantity', function(count) {
        let ops = '';
        for (var i = 1; i < count+1; i++) {
            ops += '<option value="'+i+'">'+i+'</option>';
        }
        return new Handlebars.SafeString(ops);
    });
};

