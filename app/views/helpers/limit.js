const Handlebars = require('handlebars');

module.exports = function(){
    Handlebars.registerHelper('lim', function(text) {
        return text.slice(0, 23) + (text.length > 23 ? '...' : '');
    });
};

