'use strict';
const Handlebars = require('handlebars');

module.exports = function(){
    Handlebars.registerHelper('select', function(selected, options) {
        console.log(selected);
        return options.fn(this).replace(
            new RegExp(' value=\"' + selected + '\"'),
            '$& selected="selected"');
    });
};