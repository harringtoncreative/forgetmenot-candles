const Handlebars = require('handlebars');

module.exports = function(){
    Handlebars.registerHelper('stringify', function(stuff) {
        return JSON.stringify(stuff);
    });
};

