window.Forgetmenot = window.Forgetmenot || {};
Forgetmenot.cartRemove = function(){
    'use strict';
    var p = {};
    var _p = {
        $remove: $('[data-remove]')
    };

    p.render = function(){
        var index = $(this).index();
        $(this).closest('tr').remove();
        var cookies = helpers.removeCookie(helpers.productCookieName, index);

        $(window).trigger('cookieUpdate', cookies);
    };

    _p.bindEvents = (function(){
        _p.$remove.on('click', p.render);
    }());

    return p;
};

Forgetmenot.totalPrice = function(){
    'use strict';
    var p = {};
    var _p = {
        $total: $('[data-total]'),
        $stripeTotal: $('[data-amount]')
    };


    p.render = function(){
        var total = helpers.getCookie(helpers.productCookieName).reduce(function(l,c){
            l += c.total;
            return l;
        },0);
        _p.$total.text('$'+total+'.00');
        _p.$stripeTotal.attr('data-amount', total*100);

    };

    _p.bindEvents = (function(){
        $(window).on('cookieUpdate', p.render);
    }());

    return p;
};

Forgetmenot.checkoutButton = function(){
    'use strict';
    var p = {};
    var _p = {
        $checkoutForm: $('form[name="checkout"]'),
        $checkoutButton: $('.checkout-button'),
        $stripeButton: $('.stripe-button-el')
    };

    var trigger = function(e){
        e.preventDefault();

        _p.$stripeButton = $('.stripe-button-el');
        _p.$stripeButton.trigger('click');
    };

    _p.bindEvents = (function(){
        _p.$checkoutForm.on('submit', trigger);
    }());

    return p;
};