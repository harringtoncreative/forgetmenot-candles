window.Forgetmenot = window.Forgetmenot || {};
Forgetmenot.fgmnRouter = (function(cb){
    'use strict';
    var def = $.Deferred();
    var p = {};

    var callPage = function(obj){
        for(var key in obj){ var method = obj[key];

            if (~location.pathname.indexOf(key) || key === '*') {

                if (typeof method === 'string') method = [method];

                for (var i = 0; i < method.length; i++) {
                    window.Forgetmenot[method[i]] = window.Forgetmenot[method[i]](window.Forgetmenot[method[i]], jQuery);
                }
            }
        }
        def.resolve(obj);
    };

    $(function(){
        callPage({
            '/p/':      'productPage',
            '*':        'cartBadge',
            '/cart':    ['cartRemove', 'totalPrice', 'checkoutButton'],
            '/admin/orders': 'orderStatus'
        });
    });

    return def.promise();
})();

Forgetmenot.fgmnRouter.then(function(d){
    var cookies = helpers.getCookie(helpers.productCookieName);
    cookies.unshift(null);
    Forgetmenot.cartBadge.render.apply(null, cookies);
    $(document).foundation();
});

