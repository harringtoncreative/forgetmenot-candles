window.Forgetmenot = window.Forgetmenot || {};
Forgetmenot.orderStatus = function(){
    'use strict';
    var p = {};
    var _p = {
        $form:   $('form[name="status-form"]'),
        $select:   $('select[name="status"]')
    };

    p.submitForm = function(e){
        e.preventDefault();
        $.post('/admin/update-status', {status:_p.$select.val(), id:order._id})
            .done(function(){
                var $message = $([
                    '<div class="alert success callout" data-closable style="position:fixed;width:100%;top:0;">',
                      '<h5>Updated</h5>',
                      '<button class="close-button" aria-label="Dismiss alert" type="button" data-close> <span aria-hidden="true">&times;</span></button>',
                    '</div>'
                ].join(''));
                $('body').append($message);
                setTimeout(function(){
                    $message.find('[data-close]').trigger('click');
                    setTimeout(function(){$message.remove();}, 1000);
                }, 1000);
            });
        return false;
    };


    _p.bindEvents = (function(){
        _p.$form.on('submit', p.submitForm);
    }());

    return p;
};