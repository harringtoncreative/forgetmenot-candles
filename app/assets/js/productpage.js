window.Forgetmenot = window.Forgetmenot || {};
Forgetmenot.productPage = function(){
    'use strict';
    var p = {};
    var _p = {
        $button:   $('button.add-to-cart'),
        $quantity: $('[name="quantity"]'),
        $size:     $('[name="size"]')
    };

    p.addToCart = function(){
        var cookie = helpers.addCookie(helpers.productCookieName, {
            slug:     product.slug, 
            quantity: parseInt(_p.$quantity.val(), 10),
            size:     product.sizes[_p.$size.val()]
        });

        $(window).trigger('cookieUpdate', cookie);
    };


    _p.bindEvents = (function(){
        _p.$button.on('click', p.addToCart);
    }());

    return p;
};