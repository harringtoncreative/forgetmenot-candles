window.Forgetmenot = window.Forgetmenot || {};

Forgetmenot.cartBadge = function(){
    'use strict';
    var p = {};
    var _p = {
        $cart: $('.cart-list-item')
    };

    p.render = function(e){
        var cookies = [].slice.call(arguments).slice(1);
        if (cookies) {
            var productCount = cookies.reduce(function(l,c){return l+=c.quantity;}, 0);
            _p.$cart.find('.badge').text(productCount).removeClass('hide');
            if (e) {
                _p.$cart.find('.badge').addClass('bounce') .delay(700) .queue(function(){
                   $(this).removeClass('bounce'); 
                   $(this).dequeue();
                });
            }
        }else{
            _p.$cart.find('.badge').addClass('hide');
        }
    };

    _p.bindEvents = (function(){
        $(window).on('cookieUpdate', p.render);
    }());

    return p;
};