window.Forgetmenot = window.Forgetmenot || {};
var helpers ={
    base64EmptyArray:'W10=',
    productCookieName:'products',
    addCookie: function(name, obj){
        var cookie = helpers.cookieEdit(name, function(ck){
            ck.push(obj);
        });
        return cookie;
    },
    removeCookie: function(name, index){
        var cookie = helpers.cookieEdit(name, function(ck){
            ck.splice(index, 1);
        });
        return cookie;
    },
    cookieEdit: function(name, cb){
        var cookie = helpers.getCookie(name);
        cb(cookie);
        Cookies.set(name, helpers.base64json(cookie) );
        return cookie;
    },
    getCookie:function(name){
       return helpers.base64json( Cookies.get(name) || helpers.base64EmptyArray);
    },
    base64json: function(obj){
        return typeof obj === 'string' ? JSON.parse(atob(obj)) : btoa(JSON.stringify(obj));
    }
};