'use strict';
const cursor = require('../data/products');

let utils = {};

utils.getCookie = function(request){
    let cookie;
    try{cookie = JSON.parse(new Buffer(request.state.products, 'base64').toString('ascii'));}catch(err){console.log(err);}

    if (request.state.products && cookie && cookie.length) {
        return cookie
            .map( p => {
                p.og = cursor({slug:p.slug})[0];
                p.size = p.og.sizes.filter(s => s.size === p.size.size)[0];
                p.total = p.size.price * p.quantity;
                p.prettyTotal = '$'+p.total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                return p;
            });
    }
    return [];
};

utils.getTotal = function(products){
    return products.reduce((l,c) => {
        l += c.total;
        return l;
    },0);
};



module.exports = utils;