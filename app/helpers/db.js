const low = require('lowdb');
const storage = require('lowdb/file-sync');
const db = low('app/data/db.json', { storage });

db._.mixin({val: array => array });

module.exports = db;