'use strict';
const env = require('env2')('.config/env.json');
const utils = require('../../helpers/utils');



const cart = {
    method: 'GET',
    path:'/cart',
    config:{
        handler:function (request, reply) {
            const products = utils.getCookie(request);
            const total = utils.getTotal(products);
            if (products && products.length) {
                reply.view('cart/cart', {products: products, total:'$'+total+'.00', totalStripe:total*100, STRIPE_PK:process.env.STRIPE_PK})
                    .state('products', products, { ttl: 864e5, path: '/', encoding: 'base64json'} );
            }else{
                reply.view('cart/empty_cart');
            }


        }
    }
};

module.exports = [cart];