'use strict';
const utils = require('../../helpers/utils');
const stripe = require('stripe')(process.env.STRIPE_SK);
const moment = require('moment');
const Hoek = require('hoek');

const low = require('lowdb');
const storage = require('lowdb/file-sync');
const db = low('app/data/db.json', { storage });


const cart = {
    method: 'POST',
    path:'/charge',
    config:{
        handler:function (request, reply) {
            const _id = require('node-uuid').v4();
            const products = utils.getCookie(request);
            const amount = utils.getTotal(products);
            const fortNight = moment().add(14, 'days').format("MMM Do YYYY");
            const simpleProducts = products.map(p => {
                delete p.og;
                return p;
            });
            let customerInfo = Hoek.applyToDefaults({}, request.payload);
            delete customerInfo.stripeToken;

            stripe.charges.create({
                card: request.payload.stripeToken,
                currency: 'usd',
                amount: amount*100,
                metadata:{
                    userId: _id
                }
            },
            function(err, charge) {
                if (err) {
                    console.error(err);
                    reply.view('cart/badcc', {products, total:'$'+amount+'.00'});
                } else {
                    db('orders').push({
                        _id,
                        status:'new',
                        date: +new Date(),
                        products: simpleProducts,
                        customerInfo,
                        currency: 'usd',
                        amount: amount*100,
                        taxToSetAside: (amount*100) / 0.075
                    });

                    reply.view('cart/thankyou', {products, total:'$'+amount+'.00', date: fortNight, customerInfo})
                    .state('products', [], { ttl: 864e5, path: '/', encoding: 'base64json'} );
                }
            });
            
        }
    }
};

module.exports = [cart];