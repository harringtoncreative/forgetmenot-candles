module.exports = {
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: '.',
            listing: !!process.env.DEBUG
        }
    }
};