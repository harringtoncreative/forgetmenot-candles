'use strict';
var Joi = require("joi");
const db = require('../../helpers/db');
const moment = require('moment');

const orders = {
    method: 'GET',
    path:'/admin/orders',
    config:{
        handler:function (request, reply) {
            if (request.query.password === 'La2LDVFtUpdXu7NeZhmRCt5vyPJwCP3N') {
                const pass = request.query.password;
                
                const orders = db('orders').chain().each((order)=>{
                    order.prettyDate = moment(order.date).format('MMMM Do YYYY, ~hA');
                    order.shipBy = moment(order.date).add(7, 'days').diff(moment(), 'days');
                }).sortBy('date').reverse().value();

                reply.view('orders/orders', {orders, pass});
            }   
        }
    }
};


module.exports = [orders];