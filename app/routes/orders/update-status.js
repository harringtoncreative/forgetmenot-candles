'use strict';
var Joi = require("joi");
const db = require('../../helpers/db');
const moment = require('moment');

const order = {
    method: 'POST',
    path:'/admin/update-status',
    config:{
        handler:function (request, reply) {
            let orders = db('orders');
            let order = orders.filter((order)=>{
                return order._id === request.payload.id; 
            })[0];
            order.status = request.payload.status;

            reply({order});
        }
    }
};


module.exports = [order];