'use strict';
var Joi = require("joi");
const db = require('../../helpers/db');
const moment = require('moment');

const order = {
    method: 'GET',
    path:'/admin/orders/{order_id}',
    config:{
        handler:function (request, reply) {
            if (request.query.password === 'La2LDVFtUpdXu7NeZhmRCt5vyPJwCP3N') {
                const pass = request.query.password;
                
                const order = db('orders').filter((order)=>{return order._id === request.params.order_id; })[0];

                reply.view('orders/order', {order, pass});
            }   
        }
    }
};


module.exports = [order];