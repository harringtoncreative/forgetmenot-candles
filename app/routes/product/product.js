'use strict';


const product = {
    method: 'GET',
    path:'/p/{slug}',
    config:{
        handler:function (request, reply) {
            const slug = request.params.slug ? encodeURIComponent(request.params.slug) : '404';
            const data = require('../../data/products')({slug: slug})[0];
            if (!data || slug === '404') { return reply.view('products/product404'); }

            reply.view('products/product', {product:data});
        }
    }
};

module.exports = [product];