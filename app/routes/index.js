'use strict';
const Hoek = require('hoek');
const atd = Hoek.applyToDefaults;
const data = require('../data/products')('*');

const index = {
    method: 'GET',
    path:'/',
    config:{
        handler:function (request, reply) {
            reply.view('products/products', {products:data});
        }
    }
};


module.exports = [
    index,
    atd(index, { path: '/index' }),
    atd(index, { path: '/index.html' }),
    atd(index, { path: '/home' })
];