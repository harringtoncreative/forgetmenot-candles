'use strict';
let products = [
    {
        'name':'Apple Harvest',
        'desc':'Heavenly apple orchard sent',
    },
    {
        'name':'Pumpkin Delight',
        'desc':'Rich holiday pumpkin smell',
    },
    {
        'name':'Clean Cotton',
        'desc':'Fall in love with a clean subtle sent that fills your house with freshness',
    },
    {
        'name':'Cookie Delight',
        'desc':'Enjoy the rich smell of sugar cookies with this luscious smelling candle',
    },
];











products.forEach(function(prod){
    prod.slug = prod.name.toLowerCase().replace(/\s/, '-');
    prod.sizes = [{price:12, size:'12oz'}, {price:20, size:'24oz'}];
    prod.priceStart = '12';
    if (!prod.img) {
        prod.img ='http://placehold.it/416x478';
    }
});

module.exports = (query) => {
    if (query === '*') { return products; }
    if (query) {
        let key =  Object.keys(query)[0];
        return products.filter(p => p[key] === query[key]);
    }
    
    return products;
};