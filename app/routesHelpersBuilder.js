'use strict';
const Hoek = require('hoek');
const Path = require('path');
const fs = require('fs');
const recursive = require('recursive-readdir-synchronous');

let multyDementionalRoutes = recursive(Path.join(__dirname, 'routes')).map( file => require(file) );

module.exports = Hoek.flatten(multyDementionalRoutes);


// Helpers Builder
let helpersPath = Path.join(__dirname, 'views/helpers');
fs.readdir(helpersPath, (err, files) => {
    if (err) throw err;
    files.forEach(f => require( Path.join(helpersPath, f) )() );
});