module.exports = function(grunt) {


  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: 'app/assets/js/**/*.js',
        dest: 'app/public/js/app.min.js',
      },
    },
    uglify: {
      options: {
        mangle: false
      },
      my_target: {
        files: {
          'app/public/js/app.min.js': ['app/public/js/app.min.js']
        }
      }
    },
    stylus: {
      options:{
        'include css':true,
        compress: false
      },
      compile: {
        files: {
          'app/public/css/app.min.css': 'app/assets/css/app.styl'
        }
      }
    },
    watch: {
      scripts: {
        files: ['app/assets/**/*'],
        tasks: ['build'],
        options: {
          spawn: false,
        },
      },
    }

  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-stylus');  
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['build','watch']);
  grunt.registerTask('build', ['concat','stylus']);
  grunt.registerTask('probuild',['concat','uglify', 'stylus']);

};