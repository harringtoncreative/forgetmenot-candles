'use strict';

const Path = require('path');
const appPath = Path.join(__dirname, 'app');

const Hapi = require('hapi');
const Handlebars = require('handlebars');
const env = require('env2')('./.config/env.json');
const allRoutes = require( Path.join(appPath, 'routesHelpersBuilder') );

let server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: Path.join(appPath, 'public')
            }
        }
    }
});

server.connection({port: process.env.PORT});

server.register([
    require('inert'), 
    require('vision')
], (err) => {if (err) console.error(err);});

server.views({
    engines: {
        html: Handlebars
    },
    relativeTo: appPath,
    path: 'views',
    layout: 'default',
    layoutPath: Path.join(appPath, 'views/layouts'),
    partialsPath: Path.join(appPath, 'views/partials'),
    isCached: process.env.NODE_ENV === 'test'
});

server.route(allRoutes);

server.ext('onPreResponse', function(request, reply) {
    let response = request.response || {};
    if (response.isBoom) return reply('That Page does not exsist <a href="/">Click Here to go home</a>');
    if (response.source && response.source.template) {
        response.source.context = response.source.context  || {};
        response.source.context.allProducts = require('./app/data/products')('*');
    }
    
    return reply.continue();
});


server.start(function(){
  console.log('Server was started. Running at '+ server.info.uri);
});
